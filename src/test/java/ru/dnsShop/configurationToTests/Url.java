package ru.dnsShop.configurationToTests;

public enum Url {
    COMMON("src/test/resources/configuration.properties"),
    CHECKPRICE("src/test/resources/checkingPriceWithGuarantee.properties"),
    CHECKFILTERS("src/test/resources/checkingSpecifiedFilters.properties");

    private String url;


    Url(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}