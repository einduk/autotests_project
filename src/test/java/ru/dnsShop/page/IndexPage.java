package ru.dnsShop.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexPage {

    public WebDriver driver;

    @FindBy(xpath = "//*[contains(text(), 'Смартфоны и гаджеты')]")
    private WebElement smartfonesAndGadgetsLink;

    public IndexPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver; }

    public void clickSmartphonesAndGadgets() {
        smartfonesAndGadgetsLink.click(); } }
