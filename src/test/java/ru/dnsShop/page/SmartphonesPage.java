package ru.dnsShop.page;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class SmartphonesPage {
    @FindBy(linkText = "2020 года")
    private WebElement filter2020;
    @FindBy(xpath = "//div[@data-block-id='f[pqc]']")
    private WebElement yearOfIssue;
    @FindBy(xpath = "//div[@data-block-id='price']")
    private WebElement price;
    @FindBy(xpath = "//div[@data-block-id='brand']")
    private WebElement brand;
    @FindBy(xpath = "//div[@data-id='brand']")
    private WebElement brandVisible;
    @FindBy(xpath = "//div[@data-id='f[9a9]']")
    private WebElement memory;
    @FindBy(xpath = "//button[@data-role='filters-submit']")
    private WebElement confirmButton;

    public WebDriver driver;

    public SmartphonesPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void scrollingPageDown() {
        for (int i = 0; i < 600; i++) {
            ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,10)", "");
        }
    }

    private void scrollToBrandBlock() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        try {
            jse.executeScript("arguments[0].scrollIntoView({\n" +
                    "            behavior: 'auto',\n" +
                    "            block: 'center',\n" +
                    "            inline: 'center'\n" +
                    "        });", brand);
        } catch (NoSuchElementException e) {
            jse.executeScript("arguments[0].scrollIntoView({\n" +
                    "            behavior: 'auto',\n" +
                    "            block: 'center',\n" +
                    "            inline: 'center'\n" +
                    "        });", brandVisible);
        }
        driver.findElement(By.xpath("//span[contains(text(), 'Показать всё')]")).click();
    }

    private void scrollToMemoryAtNeeded() {
        try {
            WebElement q = driver.findElement(By.xpath("//div[@data-id='f[9a9]']//div[@class='ui-collapse__content ui-collapse__content_list ui-collapse__content_in']"));
        } catch (NoSuchElementException e) {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].scrollIntoView({\n" +
                    "            behavior: 'auto',\n" +
                    "            block: 'center',\n" +
                    "            inline: 'center'\n" +
                    "        });", memory);
            driver.findElement(By.xpath("//div[@data-id='f[9a9]']")).click();
        }
    }

    public void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({\n" +
                "            behavior: 'auto',\n" +
                "            block: 'center',\n" +
                "            inline: 'center'\n" +
                "                                 });", element);
    }

    private void scrollToPriceBlock() {
        this.scrollToElement(price);
    }

    public void clickRadioPrice() {
        this.scrollToPriceBlock();
        driver.findElement(By.xpath("//span[contains(text(), '40 001')]")).click();
    }

    public void click2020YearFilter() {
        filter2020.click();
    }

    private void scrollToYearBlock() {
        this.scrollToElement(yearOfIssue);
        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-id='f[pqc]']"))).click();
    }

    public void clickCheckboxYear(String year) {
        this.scrollToYearBlock();
        driver.findElement(By.xpath(String.format("//span[contains(text(), '%s')]", year))).click();
    }

    public void clickCheckboxForBrand(String brand) {
        this.scrollToBrandBlock();
        driver.findElement(By.xpath(String.format("//div[@data-id='brand']//span[contains(text(), '%s')]", brand))).click();
    }

    public void clickMemory(String memoryVal) {
        this.scrollToMemoryAtNeeded();
        driver.findElement(By.xpath(String.format("//div[@data-id='f[9a9]']//span[contains(text(), '%s')]", memoryVal))).click();
    }

    public void clickConfirmButton() {
        this.scrollToElement(confirmButton);
        confirmButton.click();
    }

    public void clickShowButton() {
        ((JavascriptExecutor) driver).executeScript("document.querySelector('div.apply-filters-float-btn',':before').click();");
    }

    public void clickSortingTypeSelection() {
        ((JavascriptExecutor) driver).executeScript("document.querySelector('span.top-filter__icon',':before').click();");
        driver.findElement(By.xpath("//span[contains(., 'По убыванию цены')]")).click();
    }

    /**Scrolling the page to select a random phone from the list*/
    public void chooseRandomIphone() {
        this.scrollingPageDown();
        List<WebElement> smartphones = driver.findElements(By.xpath("//div[@class = 'product-info__title-link']"));
        System.out.println(smartphones.size());
        if (smartphones.size() > 1) {
            WebElement randomSmartphones = smartphones.get(new Random().nextInt(smartphones.size()));
            this.scrollToElement(randomSmartphones);
            new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(randomSmartphones));
            randomSmartphones.click();
        }
    }
}