package ru.dnsShop.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SmartphonesAndGadgetsPage {
    public WebDriver driver;
    @FindBy(xpath = "//a[@class='subcategory__item ui-link ui-link_blue']//span[contains(text(), 'Смартфоны')]")
    WebElement smartphonesLink;

    public SmartphonesAndGadgetsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void clickSmartphonesLink() {
        smartphonesLink.click();
    }
}