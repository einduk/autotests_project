package ru.dnsShop.page;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CheckingPriceWithGuaranteePage {
    @FindBy(xpath = "//span[contains(@class,'product-card-price__current')]")
    private WebElement cost;
    @FindBy(xpath = "//select[@class='form-control']")
    private WebElement select;
    private WebDriver driver;

    public CheckingPriceWithGuaranteePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void checkingResultingCost() {
        String costText = cost.getText();
        System.out.println(costText);
        Select guarantee = new Select(select);
        guarantee.selectByVisibleText("1 год");
        String costWithGuarantee = cost.getText();
        Assert.assertNotEquals("Цена не изменилась", costText, costWithGuarantee);
    }
}
