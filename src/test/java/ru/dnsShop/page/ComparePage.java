package ru.dnsShop.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class ComparePage extends SmartphonesPage {
    private WebDriver driver;

    @FindBy(xpath = "//span[@class='base-ui-toggle__icon']")
    private WebElement showDifferentToggle;

    public ComparePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    /**
     1) Scroll to the end of the page and add the table elements to the list.
     2) Convert the list into a text array and check that the elements do not match*/
    public void findTextInCells() {
        scrollingPageDown();
        List<WebElement> cells = driver.findElements(By.xpath("//p[@class = 'group-table__product-value']"));
        System.out.println(cells.size());
        ArrayList<String> texts = new ArrayList<String>();
        for (WebElement cell : cells) {
            texts.add(cell.getText());
        }
        Object[] textsArray = texts.toArray();
        for (int i = 0; i < texts.size(); i += 2) {
            Assert.assertNotEquals(textsArray[i], textsArray[i + 1]);
        }
    }

    public void clickOnShowDifferent() {
        scrollToElement(showDifferentToggle);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(showDifferentToggle)).click();
    }
}