package ru.dnsShop.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class RandomSmartphonesPage extends SmartphonesPage {
    private static WebDriver driver;

    @FindBy(xpath = "//a[@href='/compare/']")
    private WebElement compareButton;

    public RandomSmartphonesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    /**Scrolling the page with a list of smartphones to select two random models*/
    public void findRandomElements() {
        scrollingPageDown();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        List<WebElement> checkboxes = driver.findElements(By.xpath("//div[@class = 'products-list__content']" +
                "//span[@class = 'ui-checkbox__box']"));
        System.out.println(checkboxes.size());
        int j = 0;
        if (checkboxes.size() > 1) {
            while (j < 2) {
                WebElement randomCheckbox = checkboxes.get(new Random().nextInt(checkboxes.size()));
                this.scrollToElement(randomCheckbox);
                new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(randomCheckbox));
                randomCheckbox.click();
                j++;
            }
        }
    }

    public void clickOnCompareButton() {
        scrollToElement(this.compareButton);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(compareButton)).click();
    }
}