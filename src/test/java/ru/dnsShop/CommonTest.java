package ru.dnsShop;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.dnsShop.configurationToTests.ConfigurationProperties;
import ru.dnsShop.configurationToTests.Url;
import ru.dnsShop.page.*;

import java.util.concurrent.TimeUnit;

public class CommonTest {
    /**TestCheckingPriceWithGuarantee & TestCheckingSpecifiedFilters*/
    public static WebDriver driver;
    public static IndexPage indexPage;
    public static SmartphonesAndGadgetsPage smartphonesAndGadgetsPage;
    public static SmartphonesPage smartphonesPage;
    public static CheckingPriceWithGuaranteePage checkingPriceWithGuaranteePage;
    /**TestСomparisonOfSmartphoneParameters*/
    public static RandomSmartphonesPage randomSmartphonesPage;
    public static ComparePage comparePage;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ConfigurationProperties.getProperty(Url.COMMON,"chromePath"));
        driver = driver();
/**TestСomparisonOfSmartphoneParameters*/
        indexPage = new IndexPage(driver);
        smartphonesAndGadgetsPage = new SmartphonesAndGadgetsPage(driver);
        smartphonesPage = new SmartphonesPage(driver);
        randomSmartphonesPage = new RandomSmartphonesPage(driver);
        comparePage = new ComparePage(driver);
/**TestCheckingSpecifiedFilters*/
        indexPage = new IndexPage(driver);
        smartphonesAndGadgetsPage = new SmartphonesAndGadgetsPage(driver);
        smartphonesPage = new SmartphonesPage(driver);
/**TestCheckingPriceWithGuarantee*/
        indexPage = new IndexPage(driver);
        smartphonesAndGadgetsPage = new SmartphonesAndGadgetsPage(driver);
        smartphonesPage = new SmartphonesPage(driver);
        checkingPriceWithGuaranteePage = new CheckingPriceWithGuaranteePage(driver);

    }

    public static WebDriver driver() {
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get(ConfigurationProperties.getProperty(Url.COMMON, "dns"));
        return driver;
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
        System.out.println("test completed");
    }
}
