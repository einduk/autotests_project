/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2019 года
 * С помощью радиобаттона ограничить сумму
 * С помощью фильтра установить «Производителя» = «Apple»
 * Нажать кнопку «Показать» которая всплывает при установке фильтра
 * Отсортировать по убыванию цены
 * Перейти к одному из вариантов выборки
 * С помощью выпадающего списка «Доп. Гарантия» выбрать дополнительную гарантию на 1 год
 * Удостовериться, что цена на смартфон изменилась, вывести в консоль цену гарантии.
 */
package ru.dnsShop;

import org.junit.Test;
import ru.dnsShop.configurationToTests.ConfigurationProperties;
import ru.dnsShop.configurationToTests.Url;

public class TestCheckingPriceWithGuarantee extends CommonTest {
    /**Test check a product price when we use guarantee*/
    @Test
    public void testSmartphoneComparison() {
        indexPage.clickSmartphonesAndGadgets();
        smartphonesAndGadgetsPage.clickSmartphonesLink();
        smartphonesPage.clickCheckboxYear(ConfigurationProperties.getProperty(Url.CHECKPRICE, "inputYear"));
        smartphonesPage.clickCheckboxForBrand(ConfigurationProperties.getProperty(Url.CHECKPRICE, "manufacturer"));
        smartphonesPage.clickRadioPrice();
        smartphonesPage.clickShowButton();
        smartphonesPage.clickSortingTypeSelection();
        smartphonesPage.chooseRandomIphone();
        checkingPriceWithGuaranteePage.checkingResultingCost();
    }
}
