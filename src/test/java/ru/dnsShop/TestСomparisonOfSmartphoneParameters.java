/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2019 года
 * С помощью радиобаттона ограничить сумму
 * Добавить 2 телефона к сравнению
 * Перейти в раздел «Сравнение»
 * С помощью таба(переключателя) выбрать режим «Только различающиеся параметры»
 * Удостовериться, что перестали отображаться одинаковые параметры
 * Для запуска кейса через консоль выполнить mvn test -Dtest=ClassName
 */
package ru.dnsShop;

import org.junit.Test;
import ru.dnsShop.configurationToTests.ConfigurationProperties;
import ru.dnsShop.configurationToTests.Url;

public class TestСomparisonOfSmartphoneParameters extends CommonTest {
    /**Test check of the display of individual parameters of two selected smartphones*/
    @Test
    public void testSmartphoneComparison() {
        indexPage.clickSmartphonesAndGadgets();
        smartphonesAndGadgetsPage.clickSmartphonesLink();
        smartphonesPage.clickCheckboxYear(ConfigurationProperties.getProperty(Url.COMMON, "inputYear"));
        smartphonesPage.clickRadioPrice();
        smartphonesPage.clickConfirmButton();
        randomSmartphonesPage.findRandomElements();
        randomSmartphonesPage.clickOnCompareButton();
        comparePage.clickOnShowDifferent();
        comparePage.findTextInCells();
    }
}
