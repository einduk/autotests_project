/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2020 года
 * С помощью радиобаттона ограничить сумму
 * С помощью фильтра установить «Производителя» = «Xiaomi»
 * С помощью фильтра установить «Объем встроенной памяти» =  «64» и «128»
 * Нажать кнопку «Применить»
 * Удостовериться, что выборка соответствует заданным фильтрам
 * Для запуска кейса через консоль выполнить mvn test -Dtest=ClassName
 */
package ru.dnsShop;

import org.junit.Test;
import ru.dnsShop.configurationToTests.ConfigurationProperties;
import ru.dnsShop.configurationToTests.Url;

public class TestCheckingSpecifiedFilters extends CommonTest {
    /**Test check how to work filters in the system*/
    @Test
    public void testCheckingFilters() {
        indexPage.clickSmartphonesAndGadgets();
        smartphonesAndGadgetsPage.clickSmartphonesLink();
        smartphonesPage.click2020YearFilter();
        smartphonesPage.clickCheckboxForBrand(ConfigurationProperties.getProperty(Url.CHECKFILTERS, "manufacturer"));
        smartphonesPage.clickMemory(ConfigurationProperties.getProperty(Url.CHECKFILTERS, "memoryFirst"));
        smartphonesPage.clickMemory(ConfigurationProperties.getProperty(Url.CHECKFILTERS, "memorySecond"));
        smartphonesPage.clickConfirmButton();
    }
}
